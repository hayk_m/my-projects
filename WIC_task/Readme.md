# Description

For making this project was used HTML, CSS, Javascript, PHP, SQL and API request, without any framework.
The project giving ability to get some information about a country such as post code, places, longitude ,latitude and so on.

## Installation

1. Please make sure that you already have installed LAMP(Linux, Apache, MySql and PHP) on your PC.
2. Download the project from this url https://bitbucket.org/hayk_m/my-projects/src/master/WIC_task/.
3. Copy the folder WIC_task to /var/www/html directory, or make link on it.
4. Create DB "create database <database name>".
5. Inside of created database create the following to tables

 CREATE TABLE zip_codes (
     id INT(6) UNSIGNED AUTO_INCREMENT PRIMARY KEY,
     zip_code VARCHAR(30) NOT NULL,
     iso_code VARCHAR(30) NOT NULL,
     reg_date TIMESTAMP DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
     )

 CREATE TABLE places (
     id INT(6) UNSIGNED AUTO_INCREMENT PRIMARY KEY,
     zip_code VARCHAR(30) NOT NULL,
     place_name VARCHAR(30) NOT NULL,
     longitude VARCHAR(30) NOT NULL,
     latitude VARCHAR(30) NOT NULL,
     reg_date TIMESTAMP DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
     );
## Usage

Load the page, choose country ,input zip code and prese submit button.

## Page URL
http://localhost/WIC_task/