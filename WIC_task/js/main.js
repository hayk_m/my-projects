//Check... if data recorded, it will get ,if not use an api.
function getCountryInfoByZip(iso_code, zip_code) {
    if (zip_code.length == 0) {
      return;
    } else {
      var xmlhttp = new XMLHttpRequest();// new HttpRequest instance
      xmlhttp.onreadystatechange = function() {
        if (this.readyState == 4 && this.status == 200) {
            if (JSON.parse(this.responseText)['country_info'].length>0){
                createInfoTable(JSON.parse(this.responseText));
            } else {
                getInfoFromApi(iso_code, zip_code);
            }
        }
      };  
      xmlhttp.open("GET", "get_country_info.php?zip_code=" + zip_code, true);
      xmlhttp.send();
    }
  }
  function saveCountryInfoByZip(data) {
    var json_upload = "json_name=" + data;
    var xmlhttp = new XMLHttpRequest();// new HttpRequest instance
    xmlhttp.onreadystatechange = function() {
      if (this.readyState == 4 && this.status == 200) {
        let paragraph = document.querySelector("#about_information");
        let country_select = document.getElementById('country');
        paragraph.textContent = "Information of "+country_select.options[country_select.selectedIndex].text + " wasn't recorded...";
        let zip = document.getElementById('zip_code').value;
        getCountryInfoByZip(null, zip);
      }
    }
    xmlhttp.open("POST", "save_country_info.php");
    xmlhttp.setRequestHeader("Content-Type", "application/x-www-form-urlencoded");
    xmlhttp.send(json_upload);
  }
  //Get value, unset some messages and validation.
  function getZipFromInput() {
    let table = document.querySelector("#country_info");
    table.innerHTML = "";
    let paragraph = document.querySelector("#about_information");
    paragraph.textContent = "";
    let zip = document.getElementById('zip_code').value;
    let iso = document.getElementById('country').value;
    if (!zip) {//Is input filled or not(validation).
      document.querySelector("#zip_code").classList.add('error');
      document.querySelector("#error_message").style.display = "block";
    } else {
      getCountryInfoByZip(iso, zip);
    }
  }
   //Table content creation **
  function createInfoTable(data){
    let table = document.querySelector("#country_info");
    table.innerHTML = "";
    let datas = Object.keys(data['country_info'][0]);
    generateTableHead(table, datas);
    generateTable(table, data['country_info']);
  }
  //**
  function generateTableHead(table, data) {
    let thead = table.createTHead();
    let row = thead.insertRow();
    for (let key of data) {
      let th = document.createElement("th");
      let text = document.createTextNode(key);
      th.appendChild(text);
      row.appendChild(th);
    }
  }
  //**
  function generateTable(table, data) {
    for (let element of data) {
      let row = table.insertRow();
      for (key in element) {
        let cell = row.insertCell();
        let text = document.createTextNode(element[key]);
        cell.appendChild(text);
      }
    }
  }
  //Use http://api.zippopotam.us Api to get information about countries.
  function getInfoFromApi(iso_code, zip_code) {
    var xmlhttp = new XMLHttpRequest();// new HttpRequest instance
      xmlhttp.onreadystatechange = function() {
        if (this.readyState == 4 && this.status == 200) {
            saveCountryInfoByZip(xmlhttp.responseText);
        } else {
          let paragraph = document.querySelector("#about_information");
          let country_select = document.getElementById('country');
          paragraph.textContent = "Please input correct zip code for "+country_select.options[country_select.selectedIndex].text;
          return;
        }
      };
      xmlhttp.open("GET", "http://api.zippopotam.us/"+iso_code+"/"+zip_code, true);
      xmlhttp.send();
  }
  //Is input filled or not(validation).
  function checkValue(e) {
    if (e.target.value.length > 0) {
      document.getElementById("zip_code").classList.remove('error');
      document.querySelector("#error_message").style.display = "none";
    }
  }